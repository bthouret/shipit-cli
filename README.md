# intello-shipit-cli

## Description

Shipit CLI is a gem created by Intello's development team.

Designed to simplify collaboration with Git and Gitlab, its main purpose is to kickstart working on an issue. Here's what it does :

1. Create a new branch (local & remote) using a standardized name.
2. Add & push a first empty commit to that branch.
3. Create a new merge request on Gitlab, with all the issue details.

## Requirements

- Git
- A `gitlab` personal access token (`https://{{ YOUR_GITLAB_HOST  }}/profile/personal_access_tokens`)
- Issues must have a label and a milestone

`intello-shipit-cli` works as well on gitlab.com as on a privately hosted instance of Gitlab

### Linux / macOS

- Ruby 2.5+

### Windows

- Docker

A .shipit.yml file must exist in `C:\Users\[my_user]\.shipit.yml` or to a path specified by `-c [path_to_file]`
SSH keys should be present in `C:\Users\[my_user]\.ssh`

Here is an example of the format of the file :

```yaml
---
endpoint:            endpoint
private_token:       private_token
```

As an example, the [endpoint for gitlab.com](https://docs.gitlab.com/ee/api/#current-status) is `https://gitlab.intello.com/api/v4`.

## Install/Update

### Linux / macOS

```bash
gem install intello-shipit-cli
```

> NOTE: If you use a ruby manager, the gem has to be installed each time you want to use it in a new ruby context.

#### PROTIP

If you use rbenv as your ruby manager, and want to easily install/update `intello-shipit-cli` across all your rubies:

1. Install [rbenv-each](https://github.com/rbenv/rbenv-each)
1. Run `rbenv each gem install intello-shipit-cli`

### Windows

Move `shipit-windows.bat` to a folder present in your environment %PATH%.
shipit-windows.bat needs to be run from a gitlab project.

#### Known issues

- Virtualization may not be enabled on certain AMD processors.
- From our experience, You may need to update to Windows build 2004 and re-install Docker or Docker Desktop to fix the above mentionned issue.

## Setup

### Linux

You must first setup `intello-shipit-cli` by running:

```bash
shipit setup
```

This will interactivly ask you for:

- Your Gitlab server instance API endpoint. If your using gitlab.com this endpoint is `https://gitlab.com/api/v4`
- Your Gitlab personal API token

The setup will create a file in your user root folder called `.shipit.yml` with this information.

### Windows

You must manually set up your `.shipit.yml` file in your home directory : `C:\Users\[your_user]\.shipit.yml`
Setup will be performed when you first run `shipit-windows.bat`.
First time use may take a few minutes to configure its Docker environment.

### PROTIP

If you have multiple Gitlab servers or account you can manually create configuration YAML files and use the `-c` option of `intello-shipit-cli` to select the proper configuration.

For example you can have a configuration file ` ~/.shipit_personal.yml` for your peronal account an to use that you would invoke `intello-shipit-cli` with: `shipit -c ~/.shipit_personal.yml work 22`.

### Check your configuration

```bash
$ shipit settings
```

### Usage

The most often used command is `work` this is the command that will create a branch and merge request based on a ticket in the project you are currently working.

Let's take an example. We have the project 'test_shipit' already checked-out and we have issue #1 (Major bug with UI) that we want to start working on.

Without `intello-shipit-cli` you would either go in the Gitlab Web UI and create a branch and then open a merge request on that branch or do the same with a bunch of git CLI commands. The resulting merge request and branch names would vary from one developer to the other and would be a pain to do things like changelogs later on.

With `intello-shipit-cli` you simply run `shipit work 1` and it will do it all for you:

```
{21:40}[2.5.0]~/Documents/Intello/source/test_shipit:master ✓ ➭ shipit -c work 1
remote:
remote: To create a merge request for bug-ac-1-major-bug-with-ui, visit:
remote:   https://gitlab.com/IndianaTux/test_shipit/merge_requests/new?merge_request%5Bsource_branch%5D=bug-ac-1-major-bug-with-ui
remote:
remote:
remote: To create a merge request for bug-ac-1-major-bug-with-ui, visit:
remote:   https://gitlab.com/IndianaTux/test_shipit/merge_requests/new?merge_request%5Bsource_branch%5D=bug-ac-1-major-bug-with-ui
remote:

****************************************
*           IT'S HIP TO SHIP           *
*                                      *
*             |    |    |              *
*            )_)  )_)  )_)             *
*           )___))___))___)\           *
*          )____)____)_____)\          *
*        _____|____|____|____\__       *
*--------\                   /---------*
*       ^^^^^^^^^^^^^^^^^^^^^^         *
****************************************
{21:41}[2.5.0]~/Documents/Intello/source/test_shipit:bug-ac-1-major-bug-with-ui ✓ ➭
```

As you can see it created the branch `bug-ac-1-major-bug-with-ui` which corresponds to the issue label (first one selected if multiple are selected), the initials of the developper working on the issue and part of the issue title.

It will also have created a matching merge request:

![alt text](doc/mr.png)

All you have to do is start coding.

## Contributing

- Create a ticket
- Request developper access
- Use `intello-shipit-cli` to start coding
- When your ready, submit your merge request to one of the maintainers

## Development Setup

### Dependencies

* rbenv or rvm
* Ruby 2.5+
* Bundler 1.10+
* [ctags](http://ctags.sourceforge.net/)

### Setup

To install or update your development environment, run `script/bootstrap`.

### Install

1. Clone the git repository.
1. Run `script/bootstrap` to install the required gems.
1. Run `script/test` to ensure your development setup is sane.
1. Run `bundle exec guard`
1. You can run a REPL: `script/console`
1. Use the CLI: `bundle exec exe/shipit`

### Testing

You can write tests using `rspec v3+` syntax in the `spec` folder. To run the tests, run `script/test`.

## License

Copyright Intello Technologies Inc, 2016-2020. Licensed under the [MIT license](/LICENSE?raw=true).

## About Intello Technologies Inc.

Intello is sponsoring this project as we believe in open-source and the open-source community.

Intello Technologies was founded by experts from hotel & IT industry backgrounds in 2003. Based in Montreal, Canada, we're more than just a services company or a simple VAR: we develop our own in-house software which we integrate with existing industry-leading hardware and then support for years to come.

Our team has in-depth knowledge of software development combined with years of sales and field management experience in the hospitality and lodging industries. Our solutions have been deployed throughout the United States, Canada, Mexico, the Caribbean and even in Africa, and our helpdesk ensures our customers are supported around the clock. Recognized as industry leaders in our fields of expertise, our company goal is to provide great solutions and support.

Find out more on our web site at <https://www.intello.com>
