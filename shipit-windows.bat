@echo off

docker info > NUL 2>&1

REM Checks if docker is running or not
if errorlevel 1 (
    echo Please install or start Docker
    exit /b 1
)

REM If a -c flag is passed as the first argument
IF [%1] == [-c] (
    
    REM If the file doesn't exist, stop here
    IF [%2] NEQ [] (
        IF NOT EXIST "%2" (
            echo %2 - File does not exist
            exit /b 1
        )
    )

    REM Otherwise copy the config file to WORKDIR of container
    xcopy /y %2 . > NUL 2>&1
)

REM Create temp dockerfile
(
    echo FROM ruby:2.5
    echo RUN gem install intello-shipit-cli
    echo WORKDIR /usr/src/project
) > %TEMP%\Dockerfile

REM Check if Ruby container already is installed locally
docker inspect --type=image ruby:2.5 > NUL 2>&1

IF errorlevel 1 (
    ECHO Please wait while we perform a first-time setup for shipit on your Windows environment. This can take several minutes.
)

REM Build Container
docker build -t "shipit-docker" -f %TEMP%\Dockerfile . > NUL 2>&1

REM Depending on is -c was passed, use the right arg number to run container
IF [%1] == [-c] (
    docker run -v %CD%:/usr/src/project ^
    -v %HOMEDRIVE%%HOMEPATH%"\authorized_keys":/root/.ssh/authorized_keys ^
    -v %HOMEDRIVE%%HOMEPATH%"\.gitconfig":/root/.gitconfig ^
    -v %HOMEDRIVE%%HOMEPATH%"\.ssh":/root/ssh_temp/ ^
    --rm shipit-docker:latest /bin/bash -c "cp -f /root/ssh_temp/* /root/.ssh/ ; chmod 0600 /root/.ssh/* ; shipit -c .shipit.yml %3 %4"
    del .shipit.yml
) else (
    docker run -v %CD%:/usr/src/project ^
    -v %HOMEDRIVE%%HOMEPATH%"\authorized_keys":/root/.ssh/authorized_keys ^
    -v %HOMEDRIVE%%HOMEPATH%"\.gitconfig":/root/.gitconfig ^
    -v %HOMEDRIVE%%HOMEPATH%"\.ssh":/root/ssh_temp/ ^
    -v %HOMEDRIVE%%HOMEPATH%"\.shipit.yml":/root/.shipit.yml ^
    --rm shipit-docker:latest /bin/bash -c "cp -f /root/ssh_temp/* /root/.ssh/ ; chmod 0600 /root/.ssh/* ; shipit -c /root/.shipit.yml %1 %2"
)

del %TEMP%\Dockerfile
