require "spec_helper"
require "tempfile"

describe Shipit::Cli::ConfigurationFile do
  let(:empty_file) { Tempfile.new(".new_shipit.yml") }
  let(:file) do
    File.new(File.join(File.dirname(__FILE__), "..", "..", "support", "files", "shipit.yml"))
  end

  let(:valid_configuration_hash) do
    { endpoint: "https://www.example.com/api", private_token: "foobar" }
  end

  context "no file" do
    subject(:config_file) { Shipit::Cli::ConfigurationFile.new("/foo/bar/shipit.yml") }

    describe "#exist?" do
      it "is false" do
        expect(config_file.exist?).to be false
      end
    end
  end

  context "existing file" do
    subject(:config_file) { Shipit::Cli::ConfigurationFile.new(file.path) }

    describe "#exist?" do
      it "is true" do
        expect(config_file.exist?).to be true
      end
    end

    describe "#endpoint" do
      it "is initialized with value" do
        expect(config_file.endpoint).to eq "https://gitlab.com/api/v4"
      end
    end

    describe "#private_token" do
      it "is initialized with value" do
        expect(config_file.private_token).to eq "supersecret"
      end
    end
  end

  context "new file" do
    subject(:config_file) { Shipit::Cli::ConfigurationFile.new(empty_file.path, valid_configuration_hash) }

    describe "#persist" do
      it "persists a configuration" do
        config_file.persist
        expect(config_file.endpoint).to eq "https://www.example.com/api"
        expect(config_file.private_token).to eq "foobar"
      end
    end
  end
end
