require "spec_helper"

describe Shipit::Cli do
  it "has a version number" do
    expect(Shipit::Cli::VERSION).not_to be nil
  end

  context "Gem Configuration" do
    describe ".config" do
      it "is an instance of a the Configuration class" do
        expect(Shipit::Cli.config).to be_a Shipit::Cli::Configuration
      end

      it "gets a configuration value" do
        expect { Shipit::Cli.config.endpoint }.to_not raise_error
      end
    end

    describe ".configure" do
      it "should set a configuration parameter" do
        Shipit::Cli.configure endpoint: "https://example.com/api"
        expect(Shipit::Cli.config.endpoint).to eq("https://example.com/api")
      end
    end
  end
end
