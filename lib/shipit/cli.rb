require "shipit/cli/configuration"
require "shipit/cli/configuration_file"
require "shipit/cli/sanitizer"
require "shipit/cli/git"
require "shipit/cli/server"
require "shipit/cli/version"
require "shipit/cli/work"

module Shipit
  module Cli
    class << self
      # Keep track of the configuration values set after a configuration
      # has been applied
      #
      # @example Return a configuration value
      #   Shipit::Cli.config.foo #=> "bar"
      #
      # @return [Object] the configuration object
      #
      def config
        @config ||= Shipit::Cli::Configuration.new
      end

      def configure(attributes = {})
        config.apply attributes
      end

      def ascii
        label = "*" + @config.motd_list.sample[0..37].upcase.center(38) + "*"

        puts "****************************************"
        puts label
        puts "*                                      *"
        puts "*             |    |    |              *"
        puts "*            )_)  )_)  )_)             *"
        puts "*           )___))___))___)\\           *"
        puts "*          )____)____)_____)\\          *"
        puts "*        _____|____|____|____\\__       *"
        puts "*--------\\                   /---------*"
        puts "*       ^^^^^^^^^^^^^^^^^^^^^^         *"
        puts "****************************************"
      end
    end
  end
end
