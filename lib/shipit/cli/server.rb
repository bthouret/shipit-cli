require "json"
require "uri"
require "net/http"
require "net/https"

module Shipit
  module Cli
    class Server
      def initialize(url, options = {})
        @uri = URI(url)
        @scheme = @uri.scheme || "https"
        @method = options[:method] || :get
        @body = options[:body]
        @private_token = options[:private_token] || Shipit::Cli.config.private_token
        @authorization_header = "Token #{@private_token}"
        @http = set_http(@uri, @scheme)
      end

      def request
        case @method
        when :post
          @request = Net::HTTP::Post.new(@uri.request_uri, initheader = { "Content-Type" => "application/json" })
          @request.body = @body.to_json
        else
          @request = Net::HTTP::Get.new(@uri.request_uri)
        end

        @request["authorization"] = @authorization_header
        @request
      end

      def response
        @response ||= @http.request(request)
      end

      private

      def set_http(uri, scheme)
        http = Net::HTTP.new(uri.host, uri.port)
        if scheme == "https"
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_PEER
        end
        http
      end
    end
  end
end
